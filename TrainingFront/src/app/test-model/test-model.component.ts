import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TestService } from '../services/test.service';
import { Test } from './test';

@Component({
  selector: 'app-test-model',
  templateUrl: './test-model.component.html',
  styleUrls: ['./test-model.component.scss']
})
export class TestModelComponent implements OnInit,OnChanges {

  tests:Test[]=[];

  constructor(
    private testService:TestService,
    private router:Router,
  ) { }
  ngOnChanges(): void {
    if(this.tests){
      this.testService
    .getTests()
    .subscribe(resultTests =>this.tests=resultTests);
    }
  }

  ngOnInit(): void {
    this.testService
    .getTests()
    .subscribe(resultTests =>this.tests=resultTests);
  }

}
