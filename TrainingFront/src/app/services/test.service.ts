import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Test } from '../test-model/test';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class TestService {
  readonly baseUrl= "https://localhost:44336";
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  getTests():Observable<Test[]>{
    return this.httpClient.get<Test[]>(this.baseUrl + "/tests",this.httpOptions);
  }
}
