﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingAPI.DtoModels
{
    public class TestDto
    {
        public Guid id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
