﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainingAPI.DtoModels;
using TrainingAPI.EntityModels;
using TrainingAPI.Services;

namespace TrainingAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TestController : ControllerBase
    {
        ITestCollectionService _testCollectionService;
        private IMapper _mapper;

        public TestController(ITestCollectionService testCollectionService,IMapper mapper)
        {
            _testCollectionService=testCollectionService ?? throw new ArgumentNullException(nameof(testCollectionService));
            _mapper = mapper;
        }
        [HttpGet("/tests")]
        public async Task<IActionResult> GetTests()
        {
            List<TestEntity> tests = await _testCollectionService.GetAll();
            List<TestDto> testsDto = _mapper.Map<List<TestEntity>, List<TestDto>>(tests);
            //putem face validari aici
            return Ok(testsDto);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTest([FromBody] TestDto testDto)
        {
            
            if (testDto == null)
                return BadRequest("Test can't be null");
            TestEntity testEntity = _mapper.Map<TestDto, TestEntity>(testDto);
            await _testCollectionService.Create(testEntity);
            return Ok(testDto);
        }
    }
}
