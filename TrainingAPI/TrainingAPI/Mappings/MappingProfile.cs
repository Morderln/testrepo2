﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainingAPI.DtoModels;
using TrainingAPI.EntityModels;

namespace TrainingAPI.Mappings
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            //se adauga aici maparile dorite cu CreateMap<tip1,tip2>()
            CreateMap<TestEntity, TestDto>();
            CreateMap<TestDto, TestEntity>();

        }
    }
    
}
