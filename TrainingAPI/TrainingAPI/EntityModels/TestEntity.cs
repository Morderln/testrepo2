﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrainingAPI.EntityModels
{
    public class TestEntity
    {
        [BsonRepresentation(BsonType.String)]
        public Guid id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
