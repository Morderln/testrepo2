﻿using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainingAPI.EntityModels;
using TrainingAPI.Settings;

namespace TrainingAPI.Services
{
    public class TestCollectionService : ITestCollectionService
    {
        private readonly IMongoCollection<TestEntity> _tests;

        public TestCollectionService(IMongoDBSettings mongoDBSettings)
        {
            MongoClient client = new MongoClient(mongoDBSettings.ConnectionString);
            IMongoDatabase database = client.GetDatabase(mongoDBSettings.DatabaseName);

            _tests = database.GetCollection<TestEntity>(mongoDBSettings.TestingCollectionName);
        }

        public async Task<bool> Create(TestEntity test)
        {
            await _tests.InsertOneAsync(test);
            return true;
        }

        public Task<bool> Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<TestEntity> Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<TestEntity>> GetAll()
        {
            IAsyncCursor<TestEntity> result = await _tests.FindAsync(test => true);
            return result.ToList();
        }

        public Task<List<TestEntity>> GetAllByFirstName(string firstName)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Update(Guid id, TestEntity model)
        {
            throw new NotImplementedException();
        }
    }
}
