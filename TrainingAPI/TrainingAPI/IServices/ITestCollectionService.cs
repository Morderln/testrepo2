﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrainingAPI.EntityModels;
using TrainingAPI.IServices;

namespace TrainingAPI.Services
{
    public interface ITestCollectionService:ICollectionService<TestEntity>
    {
        Task<List<TestEntity>> GetAllByFirstName(string firstName);
    }
}
